# Starbreaker

This repository holds all notes, design documentation, and drafts for Starbreaker, a literary rock opera.

Please direct all inquiries to [Matthew Graybosch](mailto:mgraybosch@fastmail.com).