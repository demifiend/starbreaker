STARBREAKER OVERVIEW

Matthew Graybosch (mail@matthewgraybosch.com) 
11 June 2018


INTRODUCTION

This document exists to define the structure of the latest incarnation of my
Starbreaker project.  This is me punching the reset button.  Nothing from
previous versions is sacred, though I will salvage everything that's worth
salvaging.

THE STRUCTURE OF STARBREAKER

STARBREAKER is one epic novel which should be written so that it can be
divided into four volumes containing two relatively short novels each.  I'm
looking at a minimum total word count of 400,000.

This should be feasible, given that my first draft of STARBREAKER from 2009
weighed in at 289,000 words.

Each volume should weigh in at least 100,000 words and contain at least two
novels at least 50,000 words long.  Each novel should have ten chapters of
at least 5,000 words in length.

The four volumes should bear the following working titles:

1. SPURIOUS PARAGON
2. HEARTLESS SAVIOR
3. SHATTERED GUARDIAN
4. RELENTLESS INSURGENT

Though I intend to write STARBREAKER as a single novel, the subdivisions are
for the benefit of any publisher who might take an interest along the way.
Publishing an entire 400,000+ word novel at once by an author whose previous
attempts at publication haven't exactly been successful might be a hard
sell. Better to be able to say that it can be divided into four or even
eight parts depending on the market.

Doing this might work better for me if I end up going indie and publishing
this myself, too.

